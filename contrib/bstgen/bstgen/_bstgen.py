#!/usr/bin/env python3

#  Copyright Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Antoine Wacheux <awacheux@bloomberg.net>

import os
from os import path

def bstgen(output_directory, num_file, file_size, num_project, shape):
    os.makedirs(output_directory, exist_ok=True)

    ic_dir = "elements"
    os.makedirs(path.join(output_directory, ic_dir))

    for project in range(0, num_project):
        project_dir = path.join(output_directory, "files", "project_{}".format(project))
        os.makedirs(project_dir)

        for file in range(0, num_file):
            file_dir = path.join(project_dir, "file_{}".format(file))

            with open(file_dir, mode='wb') as f:
                f.write(bytes(file_size))

        bst_file = (
            "kind: import\n"
            "sources:\n"
            "  - kind: local\n"
            "    path: files/project_{project}\n"
            "config:\n"
            "  target: /project_{project}\n".format(project=project))
        if shape == "line" and project > 0:
            bst_file += ("depends:\n"
                         "  - filename: project_{number}.bst\n    type: build\n".format(number=project-1))

        with open(path.join(output_directory, ic_dir, "project_{}.bst".format(project)), mode='w', encoding='utf-8') as bst:
            bst.write(bst_file)

    bst_compose = ("kind: compose\n"
                   "depends:\n")
    for project in range(0, num_project):
        bst_compose += "  - filename: project_{}.bst\n    type: build\n\n".format(project)
    
    with open(path.join(output_directory, ic_dir, "build_all.bst"), mode="w", encoding="utf-8") as build_all:
        build_all.write(bst_compose)

    project_conf = "name: generated_ic\nelement-path: elements/"

    with open(path.join(output_directory, "project.conf"), mode='w', encoding='utf-8') as project_file:
        project_file.write(project_conf)
