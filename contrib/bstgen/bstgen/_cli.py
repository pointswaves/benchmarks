#!/usr/bin/env python3

#  Copyright Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Antoine Wacheux <awacheux@bloomberg.net>

import argparse
import os
from os import path
from . import bstgen

def cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--num-file", type=int, help="number of files per project", required=True)
    parser.add_argument("-f", "--file-size", type=int, help="size of project files in bytes", required=True)
    parser.add_argument("-p", "--num-project", type=int, help="number of projects to generate", required=True)
    parser.add_argument("-s", "--shape", help="shape of the dependency tree", required=True, choices=['dots', 'line'])
    parser.add_argument("output_directory")

    args = parser.parse_args()

    output_directory = path.abspath(args.output_directory)
    if not path.exists(output_directory):
        os.makedirs(output_directory, exist_ok=True)

    bstgen(output_directory, args.num_file, args.file_size, args.num_project, args.shape)
