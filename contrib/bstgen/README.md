# A BuildStream project generator

## Usage

You can generate a set of projects using:

```bash
bstgen --num-file <number of files per project> \
    --file-size <size of each file in bytes> \
    --num-project <number of projects ot generate> \
    --shape <dots|line> \
    <output_directory>
```

**Shape** : The `shape` parameter controls how projects are related to each
other. `dots` means that they will be independent and that they can be built in
parallel. `line` means that every project will depend on the previous one
and must be built sequentially.

### Example

Let's generate 3 projects using the `line` shape. Each of these projects will contain 10 files of 1Kb.
```bash
bstgen --num-file 10 --file-size 1024 --num-project 3 --shape line target_directory
```

Once done, `target_directory` will contain:
```
target_directory
    - integration_candidate
        - build_all.bst
        - project_0.bst
        - project_1.bst
        - project_2.bst
        - project.conf
    - project_0
        <project_0's content>
    - project_1
        <project_1's content>
    - project_2
        <project_2's content>

```

### Building the generated projects

To build the generated projects:

```bash
cd <output directory>/integration_candidate
bst build build_all.bst
```
