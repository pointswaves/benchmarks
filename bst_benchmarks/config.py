#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Sam Thursfield <sam.thursfield@codethink.co.uk>
#        Jim MacArthur  <jim.macarthur@codethink.code.uk>

import yaml


class BstVersionSpec():
    """Specifies a version of BuildStream to be tested.

    Note that the test harness uses Docker to ensure that the necessary
    dependencies for a particular version of BuildStream are made available in a
    reproducible way.

    Args:
       name (str): Identifier for this version, e.g. '1.0.0' or 'master'
       base_docker_image (str): Name of the Docker image containing
                                dependencies, or 'None' to use the default
       base_docker_ref (str): Tag or digest checksum specifing the exact version
                              of the Docker image to use. Defaults to 'name'.
       buildstream_repo (str): Git repository holding the version of
                               BuildStream to test, or 'None' to use the default
       buildstream_ref (str): Git ref pointing to the version of BuildStream to
                              be tested. Defaults to value of 'name'.
    """

    def __init__(self, name, base_docker_image=None, base_docker_ref=None,
                 buildstream_repo=None, buildstream_ref=None, buildstream_commit=None, buildstream_commit_date=None):
        self.name = name
        self.base_docker_image = base_docker_image or self.DEFAULT_BASE_DOCKER_IMAGE
        self.base_docker_ref = base_docker_ref or name
        self.buildstream_repo = buildstream_repo or self.DEFAULT_BUILDSTREAM_REPO
        self.buildstream_ref = buildstream_ref or name
        self.buildstream_commit = buildstream_commit
        self.buildstream_commit_date = buildstream_commit_date


class TestSpec():
    """Specifies a benchmark to be run.

    Each benchmark runs as a script inside one or more Docker containers that
    each contain a specific BuildStream version.

    The script can do whatever is needed for the purposes of the test. It should
    record its results by writing a JSON file. The 'measurements_file' argument
    locates this file within the container.

    Args:
        name (str): Identifier for this test.
        script (str): Commands to run for this test.
        measurements_file (str): Path where the script will record its measurements
        repeats (int): Number of times to repeat the test.
        mounts (list): Optional list of VolumeMountSpec entries
        description (str): Textual summary of the test
    """
    def __init__(self, name, script, measurements_file, repeats, mounts, description=""):
        self.name = name
        self.script = script
        self.measurements_file = measurements_file
        self.repeats = repeats
        self.mounts = [VolumeMountSpec(**m) for m in mounts]
        self.description = description

        assert self.repeats > 0


class VolumeSpec():
    """Specifies a volume that can be mounted during tests.

    The 'name' parameter is passed to Docker, so it must follow the naming
    restrictions applicable to Docker volume names.

    Args:
        name (str): Identifier for this volume. 
        description (str): Textual description of the volume.
        prepare (list): List of VolumePrepareSpec entries.
    """
    def __init__(self, name, description, prepare):
        self.name = name
        self.description = description
        self.prepare = VolumePrepareSpec(**prepare)


class VolumeMountSpec():
    """Specifies how to mount a volume for a given test run.

    Args:
        volume (str): Name of the volume to mount.
        path (str): Path inside the container where the volume should appear
    """
    def __init__(self, volume, path):
        self.volume = volume
        self.path = path


class VolumePrepareSpec():
    """Specifies how to prepare a volume for use.

    Preparation executes once per whole benchmarking run.

    Args:
        version (str): Name of the BstVersionSpec to use when preparing
        path (str): Path to mount the volume in the container doing preparation.
        script (str): Shell commands to run to do the preparation.
    """
    def __init__(self, version, path, script):
        self.version = version
        self.path = path
        self.script = script


class BenchmarkConfig():
    """ Contains all the configuration for a benchmark - a container for
    versions, tests, and volumes, and the defaults for tests and volumes.
    """
    def __init__(self):
        self.version_defaults = {
            'base_docker_image': 'docker.io/buildstream/buildstream-fedora',
            'buildstream_repo': 'https://gitlab.com/BuildStream/BuildStream',
        }

        self.test_defaults = {
            'measurements_file': '/root/measurements.json',
            'repeats': 3,
            'mounts': [],
        }
        self.test_specs = []
        self.volume_specs = []
        self.version_specs = []


# Loads a YAML file describing the test configuration to run.
#
# Returns a tuple of (version_specs, test_specs).
def load(path, benchmark_config: BenchmarkConfig):
    valid_toplevel_keys = {'version_defaults', 'versions', 'test_defaults',
                           'tests', 'volumes'}

    try:
        with open(path) as f:
            config = yaml.safe_load(f)
    except yaml.error.YAMLError as e:
        raise RuntimeError("Invalid YAML syntax in configuration: {}".format(e))

    invalid_keys = set(config.keys()) - valid_toplevel_keys
    if invalid_keys:
        raise RuntimeError("Invalid keys in configuration: {}".
                            format(', '.join(invalid_keys)))

    try:
        if 'version_defaults' in config:
            benchmark_config.version_defaults.update(config['version_defaults'])

        for entry in config.get('versions',[]):
            entry_with_defaults = benchmark_config.version_defaults.copy()
            entry_with_defaults.update(entry)
            benchmark_config.version_specs.append(BstVersionSpec(**entry_with_defaults))

        if 'test_defaults' in config:
            benchmark_config.test_defaults.update(config['test_defaults'])

        for entry in config.get('tests',[]):
            entry_with_defaults = benchmark_config.test_defaults.copy()
            entry_with_defaults.update(entry)
            benchmark_config.test_specs.append(TestSpec(**entry_with_defaults))

        for entry in config.get('volumes',[]):
            benchmark_config.volume_specs.append(VolumeSpec(**entry))
    except KeyError as e:
        raise RuntimeError("Config missing expected key: {}".format(e))
    except TypeError as e:
        raise RuntimeError("Invalid configuration: {}".format(e))

    return benchmark_config
