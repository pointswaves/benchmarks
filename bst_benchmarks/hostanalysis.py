#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Sam Thursfield <sam.thursfield@codethink.co.uk>

import collections
import os
import psutil
import subprocess


# Returns a dict containing all of the collected host info
def get_host_info():
    return {
        'total_system_memory_GiB': get_memory_info(),
        'processor_info': get_processor(),
        'kernel_release': get_kernel_info()}


# This returns a number
def get_memory_info():
    return bytes_to_gib(psutil.virtual_memory().total)


# This argument is a number
# This returns a number
def bytes_to_gib(value):  # Bytes to Gibibytes
    return round(value / (1024 * 1024 * 1024), 2)


# This returns a utf-8 string
def get_kernel_info():
    cmd = ["uname", "-r"]
    output = subprocess.check_output(cmd)
    return str(output, encoding="utf-8").rstrip()


# This returns a string
def get_processor():
    cores = 0
    with open('/proc/cpuinfo') as f:
        for line in f:
            if line.strip():
                if line.rstrip('\n').startswith('model name'):
                    model_name = line.rstrip('\n').split(':')[1]
                    cores += 1
    # This returns the model name as a string
    # This is done as the name cannot be reliably parsed
    # Due to different vendors and models having different naming conventions
    return {"cpu_model_name": model_name, "cpu_cores": cores}
