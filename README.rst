BuildStream benchmarking framework
==================================
This is the official tool for measuring the performance of
`BuildStream <https://gitlab.com/buildstream/buildstream>`_.


The 
`Benchmarks Concept Document <https://gitlab.com/BuildStream/benchmarks/blob/master/Benchmark_concept.md/>`_ 
in this repo should also be of use, where you can also find a link to the `Display of Headline Results <https://buildstream.gitlab.io/buildstream-benchmarking/>`_.

The tool operates by running a configurable set of tests against multiple
versions of BuildStream. Once complete, it outputs a set of results containing
the measurements taken in each test run and the exact versions that were tested.
This allows you to follow performance characteristics of BuildStream across
different versions, and to test potential changes against existing versions to
see how they affect the performance.

`Docker <https://www.docker.com/>`_ is used to manage the different versions of
BuildStream. Your system needs to have Docker installed in order to use this
tool, and the permissions must be correctly set for the tool to speak to the
Docker daemon.

Various Python libraries are also required by the tool, these are listed in the
``requirements.txt`` file.

You do not need to have BuildStream itself installed on the machine running the
tests, all of this is taken care of using Docker images.

In order to get useful results, it is vital that you run the benchmarking tool
on a machine with predicatable performance characteristics: ideally a real
computer (not a VM) which has no other processes using its CPU and IO resources.

Quick start
-----------

To run the tool with the default set of tests, make sure you are in the top
directory of its Git repository and type the following:

    python3 -m bst_benchmarks > results.json

This will spend a long time running tests, with status messages being printed
to stderr. You can increase the verbosity of the status output with the
``--debug`` argument.

As noted above, if you run this in a VM on a shared server or if you have other
processes consuming resources on the same machine the results will be fairly
meaningless.

Configuration format
--------------------

The benchmarks tool is controlled by a configuration file which is written in
YAML. The default configuration is in a file named
``bst_benchmarks/default.benchmark``, but you can use the ``--config-file``
option to specify a different configuration.

There are two things that must be configured for a given benchmark run: the
versions of BuildStream that are to be tested, and the exact set of tests to
run. Both need to be specified in the config file.

Here is an example with two versions configured:

.. code:: yaml

    version_defaults:
      base_docker_image: docker.io/buildstream/buildstream-fedora
      buildstream_repo: https://gitlab.com/BuildStream/BuildStream

    versions:
    - name: 1.0.0
      base_docker_ref: latest
      buildstream_ref: 1.0.0

    - name: master
      base_docker_ref: latest
      buildstream_ref: master

When run with this configuration, the tool will locally prepare two Docker
images called ``bst_benchmarks:1.0.0`` and ``bst_benchmarks:master``. Both
will use the latest version of the 'buildstream-fedora' image from the Docker
Hub as a base; but you can use different base Docker images for the different
versions of BuildStream if needed. For each prepared image, the tool manually
clones the buildstream.git repo and installs the given ref with ``pip3
install``, so it doesn't matter what version of BuildStream is installed in the
base image.

The other part of the configuration format defines the tests to be run. A test
here is just a script which is run inside each of the prepared Docker images.
It is expected to write its results in JSON format to a file inside the
container which the tool will then extract.

Here is an example test which measures the startup time of the ``bst`` process
and its memory usage:

.. code:: yaml

    test_defaults:
      measurements_file: /root/measurements.json
      repeats: 1

    tests:
    - name: Startup time
      script: |
        /usr/bin/time \
            -o /root/measurements.json \
            -f '{ "total-time": %e, "max-rss-kb": %M }' \
            -- bst --help

Note that in this example we bodge GNU Time into generating JSON output. This
is a temporary hack: we plan to add a tool that lives inside the
BuildStream.git tree that provides detailed timing results by scraping
the log output from ``bst`` invocations.

Layered configuration files
---------------------------

You can specify multiple configuration files with the
``--config-file`` or ``-c`` option. If you do this, the configuration
files will be combined in the order they are listed on the command
line.

* The ``tests``, ``versions`` and ``volumes`` lists are added together. A later
  config file cannot remove tests, versions or volumes specified in another config
  file.
* Values in ``test_defaults`` or ``version_defaults`` replace earlier values
  if they were already specified.

For example, these are two excerpts of config from two files:

.. code:: yaml

    version_defaults:
        base_docker_image: docker.io/buildstream/buildstream-fedora
        buildstream_repo: https://gitlab.com/jmacarthur/BuildStream

    versions:
    - name: version-under-test
      base_docker_ref: latest
      buildstream_ref: testing-version


.. code:: yaml

    version_defaults:
        buildstream_repo: https://gitlab.com/BuildStream/BuildStream

    versions:
    - name: master
      base_docker_ref: latest
      buildstream_ref: master

This produces two versions to test; one is a development version called
`testing-version` jmacarthur's repository and the other is `master` from the
BuildStream upstream repo. The version defaults in the first file apply to the
version in that file, so it uses ``testing-version`` from jmacarthur's
repository. The second file keeps the ``base_docker_image`` value, but
overwrites ``buildstream_repo``, so it will test the ``master`` branch of the
official BuildStream repository.

This may be useful if, for example, you want to automate tests and
supply a small number of changing variables such as the version under
test, while keeping a fixed general configuration.

If you specify any configuration option on the command line, the
default configuration is never used.

Output format
-------------

Here is a shortened example of the output that the benchmark tool might produce
given the example configuration file above:

.. code:: yaml

    {
        "host_info": ...
        "versions": [
            {
                "name": "1.0.0",
                "base_docker_digest": "sha256:42bde6c23b42134e3fa07f0f7348b5e83ae1322a48fbc03192d4ad9337387259",
                "buildstream_ref": "1.0.0",
                "buildstream_commit": "f0d03c82b539d9e4413c423a82fd342f82320c63",
            },
            {
                "name": "1.0.1",
                "base_docker_digest": "sha256:42bde6c23b42134e3fa07f0f7348b5e83ae1322a48fbc03192d4ad9337387259",
                "buildstream_ref": "master",
                "buildstream_commit": "b340f995455b997995fc55277a993d5f5a1656e5",
            },
        ],
        "tests": [
            {
                "name": "Startup time",
                "results": [
                    {
                        "version": "1.0.0",
                        "repeats": 1,
                        "measurements": [
                            {
                                "total_time": 2.0,
                                "max-rss-kb": 31452,
                            }
                        ]
                    },
                    {
                        "version": "master",
                        "repeats": 1,
                        "measurements": [
                            {
                                "total_time": 1.0,
                                "max-rss-kb": 21388,
                            }
                        ]
                    }
                ]
            }
        ]
    }

This shows that we ran a single test named "Startup time" against BuildStream
1.0.0 and a commit from 'master', and that the 'master' version started up
twice as fast and used around 10MB less memory than the '1.0.0' release.

Advanced tests
--------------

The benchmark tool supports features beyond the simple "run a task and see how
long it takes" example. These are described below. See the
``bst_benchmark/defaults.benchmark`` file for a real configuration example.

Volumes
~~~~~~~

The tool can create Docker volumes and populate them with content in advance of
running any tests. This is intended to be used when tests depend on some data
that needs to be fetched from the internet, so that the fetch can be done
without being included in the benchmarking results.

You declare the volumes that you need inside the toplevel ``volumes`` key,
along with a script that prepares the data in the volume. This script is run
using a one of the containerized versions of BuildStream that was defined in
the ``versions`` section, with the volume mounted at ``path``. Here is a simple
example:

.. code:: yaml

    volumes:
    - name: my-source
      prepare:
        version: master
        path: /src
        script: |
          wget http://example.com/sourcecode.tar.gz -O /src

You can then specify that a test needs to have this volume mounted using the
``mounts`` keyword, as in this example:

.. code:: yaml

    tests:
    - name: build
      mounts:
      - volume: my-source
        path: /src
      script: |
        cd /src
        # ... build the source tarball and time how long it takes

For developers
--------------

There are some options available when running the tool from the commandline.
Run ``python3 -m bst_benchmarks --help`` for a quick reference, and see below
for more detailed descriptions.

Reusing prepared images
~~~~~~~~~~~~~~~~~~~~~~~

By default, the tool prepares clean Docker images from scratch on each run,
and deletes them once the run has finished. If you want to keep the images
around after the tool exists, pass ``--keep-images``.

Preparing the images can be a slow operation. As a developer convenience, if
you want to avoid running the preparation step each time the tool executes you
can pass ``--reuse-images`` which will skip the preparation step if a suitable
image is already available.

Note that the tool doesn't check for updates to symbolic refs if it reuses
an image in this manner -- if you want to test the 'master' branch of
BuildStream and there is an image available which contains a two month old
commit from 'master' then the tool will reuse that image, and the same applies
for the base Docker image tag.

Error reporting
~~~~~~~~~~~~~~~

In the event that a test fails against a particular version of BuildStream, the
benchmark harness will avoid repeating the test and will report the error as
part of its output rather than any measurements. Here is an example:

.. code:: yaml

    "tests": [
        {
            "name": "Startup time",
            "results": [
                {
                    "version": "1.0.0",
                    "exception": "Test script returned non-zero exit code.",
                    "output": "/bin/bash: line 1: /usr/bin/its-missing: No such file or directory",
                    "returncode": 127
                }
            ]
        }
    ]
