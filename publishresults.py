#!/usr/bin/python3

#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import argparse
import os
import tempfile
import re
import time
import shutil
from distutils.dir_util import copy_tree

# Commandline executable that takes a directory containing a number of graphs
# that are to be published in html form. It generates a number of html pages
# using a supplied template form and substituting the graph page references
# into generated copies these are then transfered to a given output path.

def main():

   directory = '.'
   graphs_directory = directory
   output_path = '.'
   template_path = '.'
   files = list()
   parser = argparse.ArgumentParser()
   parser.add_argument("-d", "--results_directory",
                       help="Directory containing results to be published",
                       type=str)
   parser.add_argument("-o", "--output_path",
                       help="Output path",
                       type=str)
   parser.add_argument("-t", "--template_path",
                       help="Template path",
                       type=str)
   args = parser.parse_args()
   
   with tempfile.TemporaryDirectory(prefix='temp_staging_location') as temp_staging_area:
      if bool(args.output_path):
         if os.path.isdir(args.output_path):
            output_path = args.output_path
         else:
            logging.error("Specified output directory does not exist", args.output_path)
            sys.exit(1)

      if bool(args.template_path):
         if os.path.isdir(args.template_path):
            template_path = args.template_path
         else:
            logging.error("Specified template path does not exist", args.template_path)
            sys.exit(1) 

      if bool(args.results_directory):
         if os.path.isdir(args.results_directory):
            directory = args.results_directory
         else:
            logging.error("Specified directory does not exist", args.directory)
            sys.exit(1)

      for entry in os.scandir(args.results_directory):
         if entry.name.endswith(".png"):
            files.append(entry)

      page_path = os.path.join(temp_staging_area, 'public')

      if not os.path.exists(page_path):
         try:
            os.mkdir(page_path)
         except OSError as exc:
            pass

      graph_path = os.path.join(page_path, 'graphs')

      if not os.path.exists(graph_path):
         try:
            os.mkdir(graph_path)
         except OSError as exc:
            pass

      # Generate graph html pages
      index = list()
      template_graph = os.path.join(template_path, 'graph_page.html')
      for entry in files:
         graph_title = entry.name.replace("_", " ").replace(".png", "")
         graph_filename = entry.name.replace("png", "html")
         new_graph_relative_path = os.path.join('graphs', entry.name)
         new_graph_absolute_path = os.path.join(graph_path, entry.name)
         web_page_absolute_path = os.path.join(page_path, graph_filename)
         web_page_relative_path = graph_filename
         shutil.copyfile(template_graph, web_page_absolute_path)
         shutil.copyfile(entry.path, new_graph_absolute_path)
         with open(web_page_absolute_path) as f:
            page = f.read()
         with open(web_page_absolute_path, 'w') as f:
            mod_page = re.sub('Title_Stub', graph_title, page)
            mod_page = re.sub('Path_Stub', new_graph_relative_path, mod_page)
            f.write(mod_page)
         index.append((web_page_relative_path, graph_title))

      # Generate index html page
      index_template_page_path = os.path.join(template_path, 'index_page.html')
      index_page_path = os.path.join(page_path, 'index_page.html')
      shutil.copyfile(index_template_page_path, index_page_path)
      with open(index_page_path, 'r') as f:
         page = f.readlines()

      new_file = ""
      for line in page:
         if 'LINK_REF_STUB' in line:
            for entry in index:
               new_file += line.replace("LINK_REF_STUB", entry[0]).replace("Link_Stub", entry[1])
         elif 'Title_Stub' in line:
            index_page_title = 'Benchmarking Results: ' + time.strftime("%Y%m%d-%H%M%S")
            new_file += line.replace('Title_Stub', index_page_title)
         else:
            new_file += line

      with open(index_page_path, 'w') as f:
         f.write(new_file)

      # Copy files to destination
      try:
         copy_tree(temp_staging_area, output_path)
      except OSError as err:
         logging.error("Unable to copy pages to target directory: {}".format(err))
         sys.exit(1)


if __name__ == "__main__":
   main()
