#!/bin/bash

BRANCH="${BRANCH_REF}"
NOW=$(date +"%F")
NOWT=$(date +"%T")
JOB_NAME="${JOB_NAME_REF}"

DEFAULT_RESULT_PATH="results_out"
CACHE_PATH="results_cache"
GRAPH_CACHE_PATH="graph_cache"
PIPELINE_CACHE_PATH="pipeline_cache"
GRAPH_PATH="$DEFAULT_RESULT_PATH/graph_set/"
RESULTS_FILE_NAME="results-$NOW-$NOWT.json"
DEFAULT_FILE_NAME="results.json"
WEB_PAGE_OUTPUT_PATH="$DEFAULT_RESULT_PATH/web_pages/$JOB_NAME"

echo "Removing $WEB_PAGE_OUTPUT_PATH"
rm -rf "$WEB_PAGE_OUTPUT_PATH"

pip3 install matplotlib

if [ $# -ne 0 ]; then
  BENCHMARK_CONF="$1"
fi

if ! [ -z "${BENCHMARK_CONFIG}" ]; then
  BENCHMARK_CONF="${BENCHMARK_CONFIG}"
fi

if [ "$BRANCH" != "master" ]; then
 python3 -m bst_benchmarks $BENCHMARK_CONF -o $DEFAULT_RESULT_PATH/$DEFAULT_FILE_NAME
 python3 graphresults.py -d $CACHE_PATH/ -i $DEFAULT_RESULT_PATH/$DEFAULT_FILE_NAME -o $GRAPH_PATH
 cat $DEFAULT_RESULT_PATH/$DEFAULT_FILE_NAME
else
 FILE="$CACHE_PATH/$RESULTS_FILE_NAME"
 python3 -m bst_benchmarks $BENCHMARK_CONF -o $FILE
 python3 graphresults.py -d $CACHE_PATH/ -o $GRAPH_PATH
fi

echo "Creating $WEB_PAGE_OUTPUT_PATH"
mkdir -p $WEB_PAGE_OUTPUT_PATH
mkdir -p "$WEB_PAGE_OUTPUT_PATH/graph_set"
mkdir -p "$WEB_PAGE_OUTPUT_PATH/public"

mv $GRAPH_PATH "$WEB_PAGE_OUTPUT_PATH/"

python3 publishresults.py -d "$WEB_PAGE_OUTPUT_PATH/graph_set/" -o "$WEB_PAGE_OUTPUT_PATH/" -t "$DEFAULT_RESULT_PATH/web_pages/"

rm -rf "$WEB_PAGE_OUTPUT_PATH/graph_set"

if [ "$BRANCH" = "master" ]; then
 cp --verbose -r "$WEB_PAGE_OUTPUT_PATH" "$PIPELINE_CACHE_PATH/public/"
fi

