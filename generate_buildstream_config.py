#!/usr/bin/python3

#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import argparse
import token_file_processing
import logging
import tempfile
import glob
import ntpath
import get_sha_commits
import generate_benchmark_config
import datetime
import shutil
import git
import os

# This command line executable acts as the central method for configuring
# the benchmark CI run. A number of elements are passed in to check that
# configuration settings are tenable - buildstream url, buildstream branch,
# cached results and previous run token.
#
# output_file - the path where the bespoke benchmark file is to be placed
# results_file - the path to where the persistent benchmark results are kept.
# token_file - the path to the token file that describes the last benchmarking run
# url_path - path to the buildstream repo.
# bs_branch - the branch of buildstream being considered.

def main():
   output_file = 'generated_config.benchmark'
   results_files = 'results_cache/'
   token_file = 'results_cache/run_token.yml'
   url_path = 'https://gitlab.com/BuildStream/buildstream.git'
   bs_branch = 'master'

   parser = argparse.ArgumentParser()
   parser.add_argument("-o", "--output_file",
                       help="Name of the benchmark configuration file that is to be generated.",
                       type=str)
   parser.add_argument("-r", "--results_path",
                       help="Path for cached results.",
                       type=str)
   parser.add_argument("-t", "--token_path",
                       help="Path for the last run token.",
                       type=str)
   parser.add_argument("-u", "--url_path",
                       help="Buildstream url path.",
                       type=str)
   parser.add_argument("-b", "--bs_branch",
                       help="Buildstream branch.",
                       type=str)
   args = parser.parse_args()

   if bool(args.output_file):
      output_file = args.output_file

   if bool(args.results_path):
      results_files = args.results_path

   if bool(args.token_path):
      token_file = args.token_path

   if bool(args.url_path):
      token_file = args.url_path

   if bool(args.bs_branch):
      bs_branch = args.bs_branch

   if not generate_buildstream_config(output_file, results_files, token_file, url_path, bs_branch):
      os.sys.exit(1)


def generate_buildstream_config(output_file, results_path, run_token_path, url_path, bs_branch):
   start_sha = ''

   try:
      # Create temporary staging area
      temp_staging_area = tempfile.mkdtemp(prefix='temp_staging_location')

      try:
         # Clone buildstream repo to temporary staging area
         repo = git.Repo.clone_from(url_path, temp_staging_area)
         # Checkout the requested buildstream branch
         repo.git.checkout(bs_branch)
      except repo.exc.GitCommandError:
         logging.error('Failure accessing requested Buildstream repo/branch')
         return False

      # Init benchmark repo based upon parent directory
      bench_mark_repo = git.Repo(search_parent_directories=True)
      
      # Requested Buildstream most recent SHA reference
      # Note this could change between starting the tests and completing
      # so a single reference is used
      buildstream_sha = os.environ.get('BUILDSTREAM_HEAD')
      if not buildstream_sha:
         logging.error('Buildstream SHA not set, using current HEAD and setting')
         buildstream_sha = bench_mark_repo.head.object.hexsha
         os.environ["BUILDSTREAM_HEAD"] = str(buildstream_sha)

      # Try to process token file, it might not exist or be accessible so catch an error
      try:
         if os.path.isfile(run_token_path):
            token_data = token_file_processing.process_token_file(run_token_path)
            print(token_data)
         else:
            # Unable to access token file - create token file to do all shas from the day prior to the
            # most recent commit on the buildstream branch.
            token_data = create_default_token_file(repo, run_token_path, bs_branch, '', bench_mark_repo)
      except OSError as err:
         logging.error('Unable to either access or generate configuration: ', results_path)
         return False
      

      # Check if results directory exists (if it doesn't we shouldn't presume to
      # create it here). Walk the .json files in the directory, if they exist
      # find the latest and use it as a reference to check the token file against.
      # If the latest file does not match the entry in the token file then it is
      # possible that an aborted processing has taken place and we need to recover.
      archive_result = ''
      if os.path.isdir(results_path):
         for dirpath, dirnames, files in os.walk(results_path):
            if files:
               list_of_files = glob.glob(results_path + '*.json')
               if list_of_files:
                  latest_file = max(list_of_files, key=os.path.getmtime)
                  archive_result = ntpath.basename(latest_file)
               else:
                  logging.info('No results files found from: ', results_path)
            else:
               logging.info('No files found from: ', results_path)
      else:
         logging.error('Results path does not exist, will not generate configuration: ', results_path)
         return False
            
      # Check that token file is valid and if the last result file according to the token data is consistent
      # with results directory then get the sha commits that are between the two including the last one. At the
      # minute if there is an inconsistency between the last results file and that specified in the token file
      #
      try:
         repo_path = repo.git.rev_parse("--show-toplevel")
         if token_file_processing.verify_token_data(token_data, archive_result, results_path, repo_path):
            if token_data[ 'build' ][ 'result' ] != archive_result:
               logging.error('Last listed result not consistent with token file: ', token_data[ 'build' ][ 'result' ], archive_result)
         else:
            logging.error('Token file fails verification, creating new')
            token_data = create_default_token_file(repo, run_token_path, bs_branch, archive_result, bench_mark_repo)

         commits = get_sha_commits.get_list_of_commits(repo_path, token_data[ 'build' ][ 'bs_branch' ], token_data[ 'build' ][ 'bs_sha' ], buildstream_sha)
         generate_benchmark_config.generate_benchmark_configuration(output_file=output_file, list_of_shas=commits, bs_branch=token_data[ 'build' ][ 'bs_branch' ])
      except Exception as err:
         logging.error('Unable to generate benchmark configuration file: ', token_data[ 'build' ][ 'result' ], archive_result)
         return False

      return True

   finally:
      shutil.rmtree(temp_staging_area)


# Function to generate a default token file if the provided one doesn't exist
# or is not consistent with current configuration
#
# repo - path to local buildstream repo (so not duplicating the clone)
# run_token_path - path to the token file
# bs_branch - the buildstream branch under consideration
# archive_result - path to the most recent benchmark result
# bench_mark_repo - path to local benchmark repo (no duplication)

def create_default_token_file(repo, run_token_path, bs_branch, archive_result, bench_mark_repo):
   logging.error("Unable to access token file attempting to process all from master: ")
   commits = list()
   thresholdtime = repo.head.object.committed_date - (60 * 60 * 24)
   for commit in repo.iter_commits(bs_branch):
      if commit.committed_date >= thresholdtime:
         commits.append(commit)
   # Try to generate token file
   token_file_processing.generate_token_file(run_token_path, str(commits[0]), bs_branch, '')

   token_data = {'build' : {'time':str(datetime.datetime.now()),
                            'bs_sha':str(commits[0]),
                            'bs_branch':bs_branch,
                            'bm_sha':str(bench_mark_repo.head.object.hexsha),
                            'bm_branch':str(bench_mark_repo.head.name),
                            'result': ''}
                }
   return token_data


if __name__ == "__main__":
   main()
